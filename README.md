#### MicroTEp

Performs transposable element (TE) detection on a set of genomes using RepeatModeler (Smit and Hubley, 2008) and LTRHarvest (Ellinghaus *et al.*, 2008).

In short, this pipeline uses the output of RepeatModeler 1.0.11 (Smit and Hubley, 2008) and LTRHarvest (Ellinghaus *et al.*, 2008) from GenomeTools 1.5.10, enriched by BLASTn 2.6.0+ (Altschul et al., 1990).
Results are filtered according to their identity (>= 0.8), sequence length (< 100bp), e-value (<= 5.3e-33) and coverage (defined as the query alignment length with removed gaps divided by the query length, >= 0.8).
TE annotation is performed using the fungal Repbase database 23.05 (Bao *et al.*, 2015) by similarity search followed by a majority rule annotation performed by `MicroTEp/bin/MRannotation.py`. Overlapping annotations are removed using `MicroTEp/bin/bpAnnotation.py`.
Unclassified TE annotations overlapping gene sequences can be filtered out when providing the genome gene bed files.

Options can be displayed using the command:

> `MicroTEp/bin/MicroTEp.sh -help`

The output files are *gff3* files located in the `work_directory/bpAnnotation/out` and `work_directory/bpAnnotation/geneFilter` when using the `-j` option (see help). 


### Requirements

To run this pipeline, you need to have installed:

- RepeatModeler 1.0.11 (version 2.0.2 also works), can be downloaded here:  http://www.repeatmasker.org/RepeatMasker/

- GenomeTools 1.5.10, can be downloaded here: http://genometools.org

- BLAST 2.6.0+, can be dowloaded here: ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/.

- bedtools v2.26.0, intructions here: https://bedtools.readthedocs.io/en/latest/content/installation.html

- parallel, see here: https://howtoinstall.co/en/parallel

- transeq from EMBOSS, see here: http://manpages.ubuntu.com/manpages/bionic/man1/transeq.1e.html

- awk needs LC_NUMERIC to be set as "en_US.UTF-8" for floating numbers (can be set permanently by including `export LC_NUMERIC="en_US.UTF-8"` in your `.bashrc` file).


## About the Repbase database (Bao *et al.*, 2015)

You will also need to download the Repbase database (Bao *et al.*, 2015) in EMBL and FASTA format (see here: https://www.girinst.org/repbase/).

This pipeline have been ran on the fungal subset of the Repbase database version 23.05.

This pipeline use a modified version of the database: the annotations were written as *TE_class:TE_order:TE_superfamily* (Wicker *et al.*, 2007) according to the table `MicroTEp/DB/TE_annotation.table`.

The pipeline also uses a domain detection performed using pfam_scan.pl (https://github.com/inab/appris/tree/master/code/opt/PfamScan, included in `MicroTEp/PFAM`) on the EMBL database with modified annotation labels.

By default, the pipeline requires the 3 Repbase-based files named as `RepBase23.05.{embl,fasta}_fngrep_annotated.{fa,pfamscan}` in `MicroTEp/DB`, but these files can be provided using the `-n`, `-x` and `-s` options (see help).




### References

Altschul, S.F., Gish, W., Miller, W., Myers, E.W., Lipman, D.J., 1990. Basic local alignment search tool. J. Mol. Biol. 215, 403–410. https://doi.org/10.1016/S0022-2836(05)80360-2

Bao, Z., Eddy, S.R., 2002. Automated de novo identification of repeat sequence families in sequenced genomes. Genome Res. 12, 1269–1276. https://doi.org/10.1101/gr.88502

Ellinghaus, D., Kurtz, S., Willhoeft, U., 2008. LTRharvest, an efficient and flexible software for de novo detection of LTR retrotransposons. BMC Bioinformatics 9, 18. https://doi.org/10.1186/1471-2105-9-18

Smit, A.F.A., Hubley, R., 2008. RepeatModeler Open-1.0. URL http://www.repeatmasker.org

Tange, 0., 2011. GNU Parallel - The Command-Line Power Tool,; login: The USENIX Magazine, February 2011:42-47.

Wicker, T., Sabot, F., Hua-Van, A., Bennetzen, J.L., Capy, P., Chalhoub, B., Flavell, A., Leroy, P., Morgante, M., Panaud, O., Paux, E., SanMiguel, P., Schulman, A.H., 2007. A unified classification system for eukaryotic transposable elements. Nat. Rev. Genet. 8, 973–982. https://doi.org/10.1038/nrg2165
