#!/usr/bin/env python
#-*- coding : utf8 -*-

'''
Author: Marine Duhamel
Contact: marine.c.duhamel@gmail.com
Date: 10/10/2018
'''

#import
import sys
import numpy as np
import re
import fnmatch
from Bio import SeqIO
# sparse matrix
from scipy.sparse import lil_matrix


#functions
def moving_average(data_set, periods):
  weights = np.ones(periods) / periods
  return np.convolve(data_set, weights, mode='same')

fastaFile = sys.argv[1] #genome fasta file
indexFile = sys.argv[2] #indexed annotation result
nbAnnot = int(sys.argv[3]) #number of different annotations found by blast
decay = int(sys.argv[4]) # to substract to get proper matrix and to add in end to get proper coordinates

contigCut = indexFile.split("/")[-1].replace(".out","")

# Dic of the contig in the genome and their length
dContig_Len={}
for seq_record in SeqIO.parse(fastaFile, "fasta"):
  dContig_Len[seq_record.id]=len(seq_record)

for i in dContig_Len :
  sys.stderr.write(contigCut + " : " + str(dContig_Len[i]) + "\n")

# Sliding window size for presence of annotation
window = 3

# Each contig is considered one after the other, the blast is browsed to find annotations on that contig
# For each matrix, the row correspond to an annotation (Gypsy,Copia...) and the col correspond to a bp of a contig
for cont in dContig_Len :
  sys.stderr.write("Running script on "+ contigCut + "\n")
  matrix_pres = lil_matrix((nbAnnot,dContig_Len[cont]+1)) #Store Presence/Absence (1/0) annotation matrix
  matrix_score = lil_matrix((nbAnnot,dContig_Len[cont]+1)) #Store the bit scores of annotations
  # Initialize start and end of annotation
  start=0
  end=0
  dAnnot={} #will contain the different kind of annotation as key and the corresponding index as value
  annotIndex=0
  with open(indexFile,"r") as f2 :
    for line in f2 :
      if line.startswith('#'):
        pass
      else :
        splitline = line.split("\t")
        contig = line.split("\t")[0] # the 1st field correspond to the contig name, must be the same as the contig considered

        if contig == cont :
          annot = line.split("\t")[3] # the 4th field correspond to the annotation, which is added to the annotation dic if not already met
          if annot in dAnnot.keys() :
            pass
          else :
            dAnnot[annot] = annotIndex
            annotIndex+=1
          start = int(line.split("\t")[1])-decay-1 # the 2nd field correspond to the beginning of the alignment
          end = int(line.split("\t")[2])-decay-1 # the 3rd field correspond to the end of the alignment
          if start > end :
            start = int(line.split("\t")[2])-decay-1
            end = int(line.split("\t")[1])-decay-1
          end = end+1 # end is incremented by one so every position will be considered to put informations in matrix
          score = float(line.split("\t")[4]) # the 5th field correspond to the bit score of the hit
          for index in range(start,end) : # put 1 if the (index)th bp is annotated with the annot
            matrix_pres[dAnnot[annot],index]=1
            if matrix_score[dAnnot[annot],index] == 0 : # for the score, if there is still nothing for that annot at this position, put the corresponding score, else keep the best score
              matrix_score[dAnnot[annot],index]=score
            else :
              if matrix_score[dAnnot[annot],index] < score :
                 matrix_score[dAnnot[annot],index]=score

  matrix_slid = lil_matrix((nbAnnot,dContig_Len[cont]+1)) # use sliding windows to compute the probability density of the presence of an annotation for each bp
  for i in dAnnot :
    matrix_slid[dAnnot[i]] = lil_matrix(moving_average(matrix_pres[dAnnot[i],:].toarray()[0], window))

  matrix_calc = lil_matrix((nbAnnot,dContig_Len[cont]+1)) # will contains the overall score for each bp and annotation
  colSum = matrix_score.sum(axis=0) # the sum of all scores for each bp is then used to find the overall score/annotation for a given bp

  # compute the overall score (between 0 and 1) for each bp and annotation, considering the density probability and the score of all the annotations at a given position
  for i_line in range(matrix_score.get_shape()[0]) :
    for i_col in range(matrix_score.get_shape()[1]) :
      if colSum[0,i_col] == 0 :
        matrix_calc[i_line,i_col] = 0
      else :
        matrix_calc[i_line,i_col] = (matrix_score[i_line,i_col]/colSum[0,i_col])*matrix_slid[i_line,i_col]

  # find the best score for each bp
  # best is a vector of the highest scores of the overall score (can be 0 if no annotation, considered latter)
  # index_best is the row index of the best score (when equal scores, the first row)
  # find the annotation corresponding to the highest score for a given bp and print the annotation in output file
  for i in range(0,dContig_Len[cont]+1) :
    best = matrix_calc[:,i].toarray().max(axis=0)
    index_best = matrix_calc[:,i].toarray().argmax(axis=0)
    for k in dAnnot.keys() : # find the corresponding annotation, do not consider null score !
      if dAnnot[k] == index_best[0] and best[0] != 0:
        tf=np.asarray(matrix_calc[:,i].toarray() == best[0]) # build a boolean vector to detect multiple best score
        unique, counts = np.unique(tf, return_counts=True) # unique: unique element in tf; counts: number of occurrence of each element in tf
        dtf=dict(zip(unique, counts)) # transform unique and counts into a dictionary with unique as keys and counts as values
        # Check if the highest score correspond to one or multiple annotations
        if dtf[True] == 1 :
          print cont + "\t" + str(i+decay+1) + "\t" + str(i+decay+1) + "\t" + k  # +1 so that first bp is 1
        else :
          print cont + "\t" + str(i+decay+1) + "\t" + str(i+decay+1) + "\tMultipleAnnotations"  # same

  sys.stderr.write("Well done, " + contigCut + " done !\n")


