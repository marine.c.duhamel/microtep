#!/bin/bash

## Purpose
# With a fasta genomes input, create a database of transposable element models specific of the genomes with RepeatModeler and LTRHarvest, annotate them and search transposable element based on the model database in the genomes

### Functions
# Die function
die () { echo "$*" 1>&2 ; exit 1; }

display_usage() {
	echo -e "\nUsage:\tMicroTEp/bin/MicroTEp_v2.sh -d <work directory> -G <genome directory> [options]"
	echo -e "Purpose: de novo prediction of TEs in genomes using LTRHarvest and RepeatModeler.\n"
	echo -e "\t-d [DIRECTORY]\tName the job directory, mandatory argument.\n"
	echo -e "\t-G [DIRECTORY]\tFasta genome location for transposable element detection, mandatory argument. Beware : all fasta files in [DIRECTORY] will be considered.\n"
	echo -e "\t-c [INTEGER]\tNumber of cpu to use for the job. Default and minimum 2.\n"
	echo -e "\t-n [FASTA FILE]\tThe transposable element database (in nucleotides) used to find annotations by similarity. Recommanded: Repbase database (tested with version 23.05). Default provided in MicroTEp/DB/RepBase23.05.fasta_fngrep_annotated.fa.\n"
	echo -e "\t-x [FASTA FILE]\tThe transposable element database (in amino acids) used to find annotations by similarity. Recommanded: Repbase database (tested with version 23.05). Default provided in MicroTEp/DB/RepBase23.05.embl_fngrep_annotated.fa.\n"
	echo -e "\t-s [FILE]\tThe Pfam database of transposable elements obtained from pfam_scan.pl running on Repbase database version 23.05. Default provided in MicroTEp/DB/RepBase23.05.embl_fngrep_annotated.pfamscan."
	echo -e "\t-a [FILE]\tThe annotation table used for correspondancy between TE databases (Repbase, RepeatModeler) and output annotation. Default provided in MicroTEp/DB/TE_annotation.table. Beware: if some annotations found by RepeatModeler are not already in this table, the given annotation will be \"unclassified\".\n"
	echo -e "\t-p [FLOAT]\tPercentage of identity for blastn run used for transposable element model enrichment. Floating point or integer value. Default 0.7.\n"
	echo -e "\t-l [INTEGER]\tLength filter on transposable element models, larger or equal to [INTEGER]. Default and recommanded 100. If you want to keep only repeat sequences smaller than [INTEGER], use [-INTEGER].\n"
	echo -e "\t-m [FILE]\tLaunch transposable element detection with a specified list of RepeatModeler and LTRHarvest models. Require file path to the merged model file. Default offset. Compatible with -L, -R or -g options (useful if additional genomes are scanned since previous run for instance).\n"
	echo -e "\t-M [FILE]\tProvides majority annotation output file from a previous run when models (-m) are provided. Require file path to the annotation output directory and specified -m option. Default offset. Compatible with -L, -R or -g options (useful if additional genomes are scanned since previous run for instance).\n"
	echo -e "\t-g [DIRECTORY]\tFasta genome location of genomes on which models have been generated on in a previous run and in which the user wants to redetect TEs based on a new model database including new genomes provided in G. To be used with -m option, the previous model database. These additional genomes will be added to the detection and used for the BLAST enrichment step. Default offset.\n"
	echo -e "\t-o\tOnly run pipeline to detect and annotate genome fasta files in directory provided by -G, to be used with -m option. Default offset.\n"
	echo -e "\t-r\tLaunch the pipeline without running RepeatModeler for the creation of transposable element model database. If you want to provide your own list of RepeatModeler models, use -R option. Default offset. Incompatible with -R option.\n"
	echo -e "\t-R [DIRECTORY]\tLaunch the pipeline without running RepeatModeler for the creation of transposable element model database, using an independant run of RepeatModeler. STRONGLY RECOMMENDED. [DIRECTORY] must contain *-families.fa files from a previous RepeatModeler run, with renamed ids (DB_named) or not (DB). If you do not want to specify RepeatModeler models, use -r option. Incompatible with -r options. Default offset.\n"
	echo -e "\t-L [DIRECTORY]\tLaunch the pipeline without running LTRHarvest for the creation of transposable element model database, using an independant run of LTRHarvest. [DIRECTORY] must correspond to the 'out' folder from a previous LTRHarvest run (in yourPreviousJobDirectory/LTRH/out). Default offset.\n"
	echo -e "\t-j [DIRECTORY]\tUse gene prediction to filter unclassified TEs overlapping genes. [DIRECTORY] must contain bed files corresponding to genes coordinates as \"contig start end\", files must have the same base name as genomes. Default offset.\n"
	echo -e "Output gff3 files will be located in work_dir/bpAnnotation/out and work_dir/bpAnnotation/geneFilter when using the -j option.\n"
	echo -e "Examples:"
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir\t# Simpler command, mandatory arguments."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24\t# Use 24 proc."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -n Repbase_nt.fa -x Repbase_aa.fa -s Repbase_pfam.pfamscan\t# Change Repbase database."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -a TEannotation.table\t# Change TE annotation table."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -p 0.8 -l 300\t# Change identity and length filtering parameters."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -r -L LTRHarvest_outdir\t# Do not perform RepeatModeler search and use a previous LTRHarvest run."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -R RepeatModeler_DBdir -L LTRHarvest_outdir\t# Use previous RepeatModeler and LTRHarvest runs."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -m previousModelDatabase.fa\t# Use a previous model database and add new genomes on which RepeatModeler and LTRHarvest are run."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -m previousModelDatabase.fa -L LTRHarvest_outdir\t# Use a previous model database and add new genomes on which only RepeatModeler is run, LTRHarvest run provided."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -m previousModelDatabase.fa -R RepeatModeler_DBdir\t# Use a previous model database and add new genomes on which only LTRHarvest is run, RepeatModeler run provided."
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -m previousModelDatabase.fa -M previousMRannotations.out -g previousGenomes_dir\t# Use a previous model database and its corresponding majority annotation output file, enriched with new models from new genomes on which RepeatModeler and LTRHarvest are run. TEs are detected in the new and previous genomes using the new model database.\n"
	echo -e "\tbash MicroTEp/bin/MicroTEp.sh -d work_dir -G genomes_dir -c 24 -m previousModelDatabase.fa -M previousMRannotations.out -o\t# Use a previous model database and its corresponding majority annotation output file and to detect and annotate TEs in new genomes on which RepeatModeler and LTRHarvest are run.\n"
}


display_version() {
	echo -e "MicroTEp/bin/MicroTEp_v2.sh: 2.3\nLast update : 23th of July 2023."
}

### Pipeline
## Check arguments

# if no argument supplied
if [  $# -eq 0 ]
	then
	die "No arguments supplied. Pipeline exiting. See -h for usage."
fi


# Mandatory arguments
dflag=false
Gflag=false
# Default: nbProc=1
cflag=false
# Repbase nucleotide database
nflag=false
# Repbase protein database
xflag=false
# PfamScan database of TEs
sflag=false
# Correspondancy table of TE databases and output TE annotations
aflag=false
# Default: pid=0.7
pflag=false
# Default: minLen=100
lflag=false
# Default: no model list specified
mflag=false
# Default: no MR annotation list specified
Mflag=false
# Default: run LTRHarvest and RepeatModeler on all G genomes
gflag=false
# Default: run on genomes in Gdir
oflag=false
# Default: run RepeatModeler
rflag=false;
small_rflag=false
big_Rflag=false
RMmodels="none"
# Default: run LTRHarvest
Lflag=false
# Default: no unclassified TE filtering for genes
jflag=false

options=':d:G:c:n:x:s:a:p:l:m:M:g:orR:L:j:hv'
while getopts $options option
	do
	case $option in
		d  ) dflag=true; dir=$OPTARG;;
		G  ) Gflag=true; Gdir=$OPTARG;;
		c  ) cflag=true; nbProc=$OPTARG;;
		n  ) nflag=true; nuclDB=$OPTARG;;
		x  ) xflag=true; protDB=$OPTARG;;
		s  ) sflag=true; PfamScanDB=$OPTARG;;
		a  ) aflag=true; corrTable=$OPTARG;;
		p  ) pflag=true; pid=$OPTARG;;
		l  ) lflag=true; minLen=$OPTARG;;
		m  ) mflag=true; previousModels=$OPTARG;;
		M  ) Mflag=true; previousMRout=$OPTARG;;
		g  ) gflag=true; gdir=$OPTARG;;
		o  ) oflag=true;;
		r  ) rflag=true; small_rflag=true;;
		R  ) rflag=true; big_Rflag=true; RMmodels=$OPTARG;;
		L  ) Lflag=true; LTRHmodels=$OPTARG;;
		j  ) jflag=true; geneDir=$OPTARG;;
		h  ) display_usage; exit;;
		v  ) display_version; exit;;
		\? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
		:  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
		*  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
	esac
done


echo "MicroTEp command line:"
echo "bash $0 $@"


## Define location of the pipeline
pipelineDir=$(dirname $(dirname $(locate "MicroTEp.sh" | head -1)))

## Path of programs used
REPEATMODELER=$(dirname $(whereis RepeatModeler | sed 's/ /\n/g' | while read line; do if [ -f "$line" ]; then v=$($line -v | awk '{print $3}');\
  if [ "$v" == "open-1.0.11" ]; then echo $line; else v=$($line -v | awk '{print $3}'); if [ "$v" == "2.0.1" ]; then echo $line; fi; fi; fi; done))
GT=$(whereis gt | sed 's/ /\n/g' |  while read line; do if [ -x "$line" ] && [ ! -d "$line" ]; then v=$(echo $($line -version) | awk '{print $3}'); \
  if [ $v == "1.5.10" ]; then echo $line; fi; fi; done)
BEDTOOLS=$(whereis bedtools | sed 's/ /\n/g' |\
  while read line; do if [ -x "$line" ] && [ ! -d "$line" ];\
  then v=$($line --version); if [ "$v" == "bedtools v2.26.0" ]; then echo $line; fi; fi; done)
BLASTX=$(whereis blastx | sed 's/ /\n/g' |  while read line; do if [ -x "$line" ] && [ ! -d "$line" ]; then v=$(echo $($line -version) | awk '{print $2}');\
  if [ "$v" == "2.6.0+" ]; then echo $line; fi; fi; done)
BLASTN=$(whereis blastn | sed 's/ /\n/g' |  while read line; do if [ -x "$line" ] && [ ! -d "$line" ]; then v=$(echo $($line -version) | awk '{print $2}');\
  if [ "$v" == "2.6.0+" ]; then echo $line; fi fi; done)
MAKEBLASTDB=$(whereis makeblastdb | sed 's/ /\n/g' |  while read line; do if [ -x "$line" ] && [ ! -d "$line" ]; then v=$(echo $($line -version) | awk '{print $2}');\
  if [ "$v" == "2.6.0+" ]; then echo $line; fi; fi; done)
PARALLEL=$(whereis parallel | sed 's/ /\n/g' |  while read line; do if [ -x "$line" ] && [ ! -d "$line" ];\
  then v=$($line -h | tail -6 | head -1 | awk '{print $1" "$2" "$3}' | sed 's/://g'); if [ "$v" == "O. Tange (2011)" ]; then echo $line; fi; fi; done)
PFAMSCAN=$pipelineDir/PFAM/PfamScan/pfam_scan.pl
TRANSEQ=transeq #$(whereis transeq | sed 's/ /\n/g' |  while read line; do if [ -x "$line" ] && [ ! -d "$line" ]; then v=$($line --version); if [ "$v" == "EMBOSS:6.6.0.0" ]; then echo $line; fi; fi; done)
PFAM=$pipelineDir/PFAM



# Check r vs R incompatibility
if $small_rflag && $big_Rflag
	then
	die "-r and -R options are incompatible. Please choose only one of them. See -h for help. Pipeline exiting."
fi

# Check m dependancy for M
if $Mflag && ! $mflag
	then
	die "-M option requires that -m option is provided. Please specify -m. See -h for help. Pipeline exiting."
fi

# Check m dependancy for o
if $oflag && ! $mflag
	then
	die "-o option requires that -m option is provided. Please specify -g. See -h for help. Pipeline exiting."
fi

# Check genome directory -G argument
if ! $Gflag
	then
	die "-G option is mandatory. Please specify the name of the directory contaning genomes for transposable element detection. See -h for help. Pipeline exiting."
fi

# Number of processors
if ! $cflag
	then
	nbProc=2
	echo "undefined nbProc argument, default nbProc=2."
fi

# Check number of proc
if [ $nbProc -eq 1 ] && (! $rflag )
	then
	die "RepeatModeler need at least 2 processor to run. Pipeline exiting."
else
	nbProcRM=$(echo "$nbProc - 1" | bc)
fi

# Check job directory -d argument
if ! $dflag
	then
	die "-d argument is mandatory. Please specify the name of the job directory. See -h for help. Pipeline exiting."
fi

# Create the job directory
if $big_Rflag
	then
	mkdir -p $dir/{RM,LTRH,log,BLASTN,annotation,bpAnnotation}
	mkdir ${dir}/RM/{DB,DB_named}
	cp -r $RMmodels ${dir}/RM
	mkdir $dir/BLASTN/{DB,out}
	mkdir $dir/annotation/{tmp,out,index}
	mkdir $dir/bpAnnotation/{tmp,out,contigIndex,genome,cutGenome}
	if ! $Lflag
		then
		mkdir $dir/LTRH/{DB,rawOut,out,tmp_scripts}
	else
		cp -r $LTRHmodels $dir/LTRH
	fi
	if $mflag
		then
		if $Mflag
			then
			cp $previousMRout $dir/annotation/out/MRannotations.out
		fi
		cp $previousModels $dir/BLASTN/models.fa
	fi
elif $small_rflag
	then
	mkdir -p $dir/{LTRH,log,BLASTN,annotation,bpAnnotation}
	mkdir $dir/BLASTN/{DB,out}
	mkdir $dir/annotation/{tmp,out,index}
	mkdir $dir/bpAnnotation/{tmp,out,contigIndex,genome,cutGenome}
	if ! $Lflag
		then
		mkdir $dir/LTRH/{DB,rawOut,out,tmp_scripts}
	else
		cp -r $LTRHmodels $dir/LTRH
	fi
	if $mflag
		then
		if $Mflag
			then
			cp $previousMRout $dir/annotation/out/MRannotations.out
		fi
		cp $previousModels $dir/BLASTN/models.fa
	fi
elif $Lflag
	then
	mkdir -p $dir/{RM,LTRH,log,BLASTN,annotation,bpAnnotation}
	mkdir $dir/RM/{DB,DB_named,tmp_scripts}
	mkdir $dir/BLASTN/{DB,out}
	mkdir $dir/annotation/{tmp,out,index}
	mkdir $dir/bpAnnotation/{tmp,out,contigIndex,genome,cutGenome}
	mkdir $dir/LTRH/out
	cp -r $LTRHmodels $dir/LTRH/out
	if $mflag
		then
		if $Mflag
			then
			cp $previousMRout $dir/annotation/out/MRannotations.out
		fi
		cp $previousModels $dir/BLASTN/models.fa
	fi
elif $mflag
	then
	if ! $rflag
		then
		mkdir -p $dir/RM/{DB,DB_named,tmp_scripts}
		echo "here"
	fi
	if ! $Lflag
		then
		mkdir -p $dir/LTRH/{DB,rawOut,out,tmp_scripts}
	fi
	mkdir -p $dir/{log,BLASTN,annotation,bpAnnotation}
	mkdir -p $dir/BLASTN/{DB,out}
	mkdir -p $dir/annotation/{tmp,out,index}
	if $Mflag
		then
		cp $previousMRout $dir/annotation/out/MRannotations.out
	fi
	mkdir $dir/bpAnnotation/{tmp,out,contigIndex,genome,cutGenome}
	cp $previousModels $dir/BLASTN/models.fa
else
	if [ ! -d $dir ]
		then
		mkdir -p $dir/{RM,LTRH,log,BLASTN,annotation,bpAnnotation}
		mkdir $dir/RM/{DB,DB_named,tmp_scripts}
		mkdir $dir/BLASTN/{DB,out}
		mkdir $dir/annotation/{tmp,out,index}
		mkdir $dir/bpAnnotation/{tmp,out,contigIndex,genome,cutGenome}
		mkdir $dir/LTRH/{DB,rawOut,out,tmp_scripts}
	else
		die "$dir directory already exists. Pipeline exiting."
	fi
fi

if $jflag
	then
	mkdir -p $dir/bpAnnotation/geneFilter/bed
fi

# Percentage identity
if ! $pflag
	then
	pid=0.8
	echo "undefined pid argument (-p), default pid=0.8."
fi

# Minimal sequence length
if ! $lflag
	then
	minLen=100
	echo "undefined minLen argument (-l), default minLen=100."
fi

# Repbase nucleotide database
if ! $nflag
	then
	nuclDB=$pipelineDir/DB/RepBase23.05.fasta_fngrep_annotated.fa
	echo "undefined Repbase nucleotide database argument (-n), default $pipelineDir/DB/RepBase23.05.fasta_fngrep_annotated.fa."
fi

# Percentage identity
if ! $xflag
	then
	protDB=$pipelineDir/DB/RepBase23.05.embl_fngrep_annotated.fa
	echo "undefined Repbase protein database argument (-x), default $pipelineDir/DB/RepBase23.05.embl_fngrep_annotated.fa."
fi

# PfamScan database
if ! $sflag
	then
	PfamScanDB=$pipelineDir/DB/RepBase23.05.embl_fngrep_annotated.pfamscan
	echo "undefined PfamScanDB argument (-s), default $pipelineDir/DB/RepBase23.05.embl_fngrep_annotated.pfamscan."
fi

# Correpondence table for TE annotations
if ! $aflag
	then
	corrTable=$pipelineDir/DB/TE_annotation.table
	echo "undefined correspondence table of TE annotations argument (-a), default $pipelineDir/DB/TE_annotation.table."
fi


## Pipeline start

# Get RM and LTRH models
# If models are provided and no other need to be built, go straight to detection and annotation
if $oflag
	then
	# do nothing
	:
	else
	if ! $rflag
		then
		RMscriptRun=$dir/RM/tmp_scripts/RMrun.sh
	fi

	LTRHscriptRun=$dir/LTRH/tmp_scripts/LTRHrunAll.sh


	# Process RepeatModeler (RM) and LTRHarvest (LTRH) on genomes in Gdir
	for genome in $(ls ${Gdir}/*.fa)
		do
		genomeName=$(basename $genome .fa)
		## RM (if !rflag)
		# Create models with RM in dedicated directory
		if ! $rflag
			then
			RMDBdir=${dir}/RM/DB/${genomeName}
			#RM database creation
			${REPEATMODELER}/BuildDatabase -name $RMDBdir $genome
			# Prepare the RM run
			echo "RepeatModeler running on $genome"
			echo "${REPEATMODELER}/RepeatModeler -engine ncbi -pa $nbProcRM -database $RMDBdir > ${dir}/RM/${genomeName}.rmout" >> $RMscriptRun
		fi

		if ! $Lflag
			then
			# Create models with LTRH in dedicated directory
			LTRHDBdir=${dir}/LTRH/DB/${genomeName}
			LTRHout=${dir}/LTRH/rawOut/${genomeName}
			LTRHscript=${dir}/LTRH/tmp_scripts/${genomeName}.sh
			# Prepare the LTRH run
			# Create the enhanced suffix array
			echo "echo \"LTRHarvest running on $genome\"" >> $LTRHscript
			echo "$GT suffixerator -db $genome -indexname $LTRHDBdir -tis -suf -lcp -des -ssp -sds -dna" >>  $LTRHscript
			# Run LTRH
			echo "$GT ltrharvest -index $LTRHDBdir -seqids yes -tabout no -out ${LTRHout}.fa -gff3 ${LTRHout}.gff3" >> $LTRHscript
			echo "bash $LTRHscript" >> $LTRHscriptRun
		fi
	done

	# Run RM (if ! $rflag)
	if ! $rflag
		then
		echo "Begin RepeatModeler run : $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
		bash $RMscriptRun
		echo "End of RepeatModeler run : $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
		# Redirect RM output
		mv RM_* ${dir}/log
		mv ${dir}/RM/*.rmout ${dir}/log
	fi

	# Change id names of RM fasta files (>rnd__shortid__annot__length) for genomes in Gdir if DB_named no provided
	if ! $small_rflag
		then
		for i in $(ls $Gdir/*.fa)
			do
			awk 'NR==1 {len=split(FILENAME,a,"/"); gsub(".fa","",a[len]); gsub(">","",$1); split($1,b,"_"); print a[len]"\t"b[1]}' $i
		done > $dir/RM/genome_id.table

		if [[ $big_Rflag && "$(basename $RMmodels)" != "DB_named" ]] || [[ ! $big_Rflag ]]
			then
			awk -v dir=$dir '{if(NR==FNR){annot[$2]=$1} else {if($1~/^>/){if(id!=""){print id"__"length(seq) >> dir"/RM/DB_named/"genome".tmp";\
				print seq >> dir"/RM/DB_named/"genome".tmp"; seq=""}; len=split(FILENAME,a,"/"); gsub("-families.fa","",a[len]); genome=a[len];\
				split($1,b,"#"); split(b[2],c," "); if(annot[c[1]]!=""){id=">rnd__"genome"__"annot[c[1]]} else {\
				id=">rnd__"genome"__unclassified:unclassified:unclassified"}} else {seq=seq""$0}}}END{\
				print id"__"length(seq) >> dir"/RM/DB_named/"genome".tmp"; print seq >> dir"/RM/DB_named/"genome".tmp"}' $corrTable $dir/RM/DB/*-families.fa
			awk -v dir=$dir '{if(NR==FNR){shortid[$1]=$2} else {len=split(FILENAME,a,"/"); gsub(".tmp","",a[len]); if($0~/^>/){\
				split($1,b,"__"); gsub(b[2],shortid[b[2]],$1); print $0 >> dir"/RM/DB_named/"a[len]"-families.fa"} else {\
				print $0 >> dir"/RM/DB_named/"a[len]"-families.fa"}}}' $dir/RM/genome_id.table $dir/RM/DB_named/*.tmp
		fi
	fi

	rm -f $dir/RM/DB_named/*.tmp

	# Run LTRH in parallel (if ! $Lflag)
	if ! $Lflag
		then
		echo "Begin LTRHarvest run : $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
		$PARALLEL -j $nbProc < $LTRHscriptRun
		echo "End of LTRHarvest run : $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"

		# Get the fasta repeat regions from LTRH (rawOut/*.gff3 only contains the LTR transposon and does not include TSD regions) (if ! $Lflag)
		LTRHrawOut=${dir}/LTRH/rawOut
		for LTRHout in $(ls ${LTRHrawOut}/*.gff3)
			do
			LTRHoutName=$(basename $LTRHout .gff3)
			if [ ! -f ${dir}/LTRH/out/${LTRHoutName}.gff3 ]
				then
				# Extract each repeat_region annotation to get the corresponding fasta with bedtools
				awk -v OFS="\t" '$3=="repeat_region" {print $0,$4,$5}' $LTRHout > ${dir}/LTRH/out/${LTRHoutName}.bed
				$BEDTOOLS getfasta -fi ${Gdir}/${LTRHoutName}.fa -bed ${dir}/LTRH/out/${LTRHoutName}.bed -fo ${dir}/LTRH/out/${LTRHoutName}.fa
			fi
		done

	fi
fi


## BLASTN search by genome

# Blastn using the LTRH and RM models against the genomes to identify missed repeated sequences
if ! $oflag
	then
	ls ${Gdir}/*.fa > ${dir}/BLASTN/DB/genomes.fofn
fi

# Concatenate models to create blast database depending on the options given
# If models are provided and no other need to be built, go straight to detection and annotation
if $oflag
	then
	:
else
	toCat=${dir}/LTRH/out/*.fa
	if ! $small_rflag
		then
		toCat=$(echo "$toCat ${dir}/RM/DB_named/*-families.fa")
	fi
	cat $toCat >> ${dir}/BLASTN/models.fa
fi


# Run blastn
for genome in $(cat ${dir}/BLASTN/DB/genomes.fofn)
	do
	# Make blastn DB for genome
	gName=$(basename $genome .fa)
	$MAKEBLASTDB -in $genome -dbtype nucl -out ${dir}/BLASTN/DB/$gName
	# Run Blastn for each genome
	echo "Begin Blastn run: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
	blastRes=${dir}/BLASTN/out/${gName}
	$BLASTN -task megablast -num_threads $nbProc -db ${dir}/BLASTN/DB/$gName -query ${dir}/BLASTN/models.fa -outfmt 6 -out ${blastRes}.bl
	echo "End of Blastn run: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"

done

# Format pid threshold
if (( $(echo "$pid < 1" | bc -l) ))
	then
	pid=$(echo "$pid * 100" | bc)
	pid=${pid%.*}
fi

cat ${dir}/BLASTN/out/*.bl > ${dir}/BLASTN/out/allGenomes.bl

echo "Begin BLASTN filtering parameter setting (e-value, 0.75 percentile): $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
totLine=$(wc -l ${dir}/BLASTN/out/allGenomes.bl | awk '{print $1}')
percentile75=$(echo "$totLine*0.75" | bc | awk '{split($1,a,"."); print a[1]}')
evalue=$(sort -k11,11g ${dir}/BLASTN/out/allGenomes.bl | awk -v percentile75=$percentile75 'NR==percentile75 {print $11}')
echo "evalue is $evalue"
echo "Begin BLASTN filtering parameter setting (e-value, 0.75 percentile): $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"



echo "Begin BLASTN filtering: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
awk -v minL=$minLen -v pid=$pid '{if($1~/^rnd/){split($1,a,"__"); split(a[2],aa,"-"); name_q=aa[1]; qlen=a[4]} else {split($1,nq,"-"); name_q=nq[1]; split($1,a,":"); \
	split(a[2],b,"-"); if(b[1]<b[2]){qstart=b[1]; qend=b[2]} else {qstart=b[2]; qend=b[1]}; qlen=(qend-qstart+1)}; split($2,ns,"-"); name_s=ns[1];\
        if(name_q==name_s){if($7<$8){qAstart=$7; qAend=$8} else {qAstart=$8; qAend=$7}; qAlen=(qAend-qAstart+1-$6);\
        if(qlen>=minL && qAlen>=minL){coverage=(qAlen/qlen);\
        if(coverage>=0.8 && $3>=pid){seen[$1]++}}}; if(line[$1]==""){line[$1]=$0} else{line[$1]=line[$1]"\n"$0}}END{\
        for (s in seen){if(seen[s]>=3){print line[s]}}}' ${dir}/BLASTN/out/allGenomes.bl |\
        awk -v pid=$pid -v minLen=$minLen -v evalue=$evalue '$3>=pid && $11<=evalue {if($1~/^rnd/){split($1,a,"__"); qlen=a[4]} else {split($1,a,":"); split(a[2],b,"-"); \
	if(b[1]<b[2]){qstart=b[1]; qend=b[2]} else {\
        qstart=b[2]; qend=b[1]}; qlen=(qend-qstart+1)}; if($7<$8){qAstart=$7; qAend=$8} else {qAstart=$8; qAend=$7}; qAlen=(qAend-qAstart+1-$6);\
        coverage=(qAlen/qlen); if($9<$10){slen=$10-$9+1} else {slen=$9-$10+1}; if(coverage>=0.8 && slen>=minLen){print $0}}' > ${dir}/BLASTN/out/allGenomes_filtered.bl
echo "End of BLASTN filtering: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"

## blast hits annotation

# Create blast DB for repbase database
searchDir=${dir}/annotation/tmp
blastxDB=${searchDir}/repbase_BlastxDB
$MAKEBLASTDB -in $protDB -dbtype prot -out $blastxDB
blastnDB=${searchDir}/repbase_BlastnDB
$MAKEBLASTDB -in $nuclDB -dbtype nucl -out $blastnDB

# Get contig-genome correspondancy
while read line
	do
	awk 'NR==1 {gsub(">","",$1); split($1,a,"_"); print FILENAME"\t"a[1]}' $line
done < $dir/BLASTN/DB/genomes.fofn > $dir/BLASTN/DB/genomes_contig.fofn

# Get TE sequences and split them to perform the annotation faster
allGenomesFasta=$searchDir/allGenomes.fa
cat $Gdir/*.fa > $allGenomesFasta
if $gflag && ! $Mflag
	then
	cat $gdir/*.fa >> $allGenomesFasta
fi

if $oflag && $Mflag
	then
	:
else
	if $Mflag
		then
		awk '{if(NR==FNR){seen[$1]++} else {if(seen[$2":"$9"-"$10]=="" && seen[$2":"$10"-"$9]==""){print}}}' $dir/annotation/out/MRannotations.out $dir/BLASTN/out/allGenomes_filtered.bl > $dir/BLASTN/out/tmp
		mv $dir/BLASTN/out/tmp $dir/BLASTN/out/allGenomes_filtered.bl
	fi

	awk '{if($9<$10){start=$9;end=$10} else {start=$10;end=$9}; seen[$2"\t"start"\t"end]++}END{for (s in seen){print s}}' $dir/BLASTN/out/allGenomes_filtered.bl |\
		bedtools sort -i > $searchDir/allGenomes_filtered.bed


	bedtools getfasta -fi $allGenomesFasta -bed $searchDir/allGenomes_filtered.bed > $searchDir/allGenomes_filtered.fa

	nbLinesTot=$(wc -l $searchDir/allGenomes_filtered.bed | awk '{print $1}')
	nbLinesSplitAt=$(echo "($nbLinesTot/$nbProc)/2" | bc)
	awk '{if($1~/^>/){if(id!=""){print id"\t"seq; seq=""}; id=$1} else {seq=seq""$1}}END{print id"\t"seq; seq=""}' $searchDir/allGenomes_filtered.fa |\
		awk -v nbLinesSplitAt=$nbLinesSplitAt -v nbLim=$nbProc/2 -v searchDir=$searchDir 'BEGIN{cpt=0;suf=1}{cpt++; if(cpt<nbLinesSplitAt || suf==nbLim){\
		print $1"\n"$2 >> searchDir"/allGenomes_filtered_"suf".fa"} else {print $1"\n"$2 >> searchDir"/allGenomes_filtered_"suf".fa"; cpt=0;suf++}}'

	echo "$nbLinesSplitAt lines per blast file to process"

	# Run 1st method for annotation: blastn using cluster sequences against repbase nucleotide database
	# Run 2nd method for annotation: blastx using cluster sequences against repbase nucleotide database
	# Run 3rd method for annotation: pfam_scan comparison between cluster sequences and Repbase protein database
	rm -f $searchDir/*.sh
	for i in $(ls $searchDir/allGenomes_filtered_*.fa)
		do
		name=$(basename $i .fa)
		echo -e "sed 's/:/---/g' $i | sed 's/_/--/g' | sed 's/>/>:/g' > $searchDir/${name}_4transeq.fa"  >> $searchDir/${name}.sh
		echo -e "$BLASTN -db $blastnDB -query $i -outfmt 6 -out $searchDir/${name}.bln"  >> $searchDir/${name}.sh
		echo -e "awk '\$11<=1e-5 && \$3>=80' $searchDir/${name}.bln > $searchDir/${name}_filtered.bln"  >> $searchDir/${name}.sh
		echo -e "$BLASTX -db $blastxDB -query $i -outfmt 6 -out $searchDir/${name}.blx"  >> $searchDir/${name}.sh
		echo -e "awk '\$11<=1e-5 && \$3>=80' $searchDir/${name}.blx > $searchDir/${name}_filtered.blx"  >> $searchDir/${name}.sh
		echo -e "$TRANSEQ -sequence $searchDir/${name}_4transeq.fa -outseq $searchDir/${name}.transeq -frame 1,2,3,-1,-2,-3"  >> $searchDir/${name}.sh
		echo -e "perl -I /usr/share/perl5/ -I $pipelineDir/PFAM/PfamScan $PFAMSCAN -cpu 2 -fasta $searchDir/${name}.transeq -dir $PFAM -outfile $searchDir/${name}.pfamscan"  >> $searchDir/${name}.sh
		echo -e "egrep -v \"#|^$\" $searchDir/${name}.pfamscan | awk '\$13<=1e-5 {split(\$1,a,\"_\"); print a[1],\$6}' | sed 's/---/:/g' | sed 's/--/_/g' > $searchDir/${name}.pfannot"  >> $searchDir/${name}.sh
		echo -e "while read line; do seq=\$(echo \$line | awk '{print \$1}'); PF=\$(echo \$line | awk '{print \$2}'); grep \"\$PF\" $PfamScanDB | awk -v seq=\$seq '{split(\$1,a,\":\"); seen[a[2]\":\"a[3]\":\"a[4]]++}END{for (s in seen){print seq\"\\\t\"s\"\\\t\"seen[s]}}'; done < $searchDir/${name}.pfannot > $searchDir/${name}.pfmatch"  >> $searchDir/${name}.sh
		echo "bash $searchDir/${name}.sh"  >> $searchDir/all_script.sh
	done

	echo "wait;" >> $searchDir/all_script.sh

	echo "Begin TE sequence annotation: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
	jobs=$(echo "$nbProc/2" | bc)
	parallel -j $jobs < $searchDir/all_script.sh
	echo "End of TE sequence annotation: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"

	MRout=$dir/annotation/out

	awk -v MRout=$MRout '{split($2,a,"_"); seen[a[1]]++}END{for (s in seen) {printf "" > MRout"/"s".blx"; printf "" > MRout"/"s".bln"; printf "" > MRout"/"s".pfmatch"; printf "" > MRout"/"s".repeatmodeler"}}' $dir/BLASTN/out/allGenomes_filtered.bl
	awk '{if($9<$10){start=$9;end=$10} else {start=$10;end=$9}; if(seen[$2"\t"start"\t"end]=="" || seen[$2"\t"start"\t"end]<$12){seen[$2"\t"start"\t"end]=$12}}END{for (s in seen) print s"\t"seen[s]}' $dir/BLASTN/out/allGenomes_filtered.bl | awk -v MRout=$MRout '{split($1,a,"_"); print $0 >> MRout"/"a[1]".bl"}'

	# Merge all annotation search result and also RM annotation
	awk -v OFS="\t" '$1~/^rnd/ && $1!~/unclassified/ {if($9<$10){start=$9;end=$10} else {start=$10;end=$9}; split($1,rm,"__"); print $2":"start"-"end,rm[3]}' $dir/BLASTN/out/allGenomes_filtered.bl | awk -v MRout=$MRout '{split($1,a,"_"); print $0 >> MRout"/"a[1]".repeatmodeler"}'
	cat $searchDir/allGenomes_filtered_*_filtered.bln | awk -v MRout=$MRout '{split($1,a,"_"); print $0 >> MRout"/"a[1]".bln"}'
	cat $searchDir/allGenomes_filtered_*_filtered.blx | awk -v MRout=$MRout '{split($1,a,"_"); print $0 >> MRout"/"a[1]".blx"}'
	cat $searchDir/allGenomes_filtered_*.pfmatch | awk -v MRout=$MRout '{split($1,a,"_"); print $0 >> MRout"/"a[1]".pfmatch"}'


	#rm -r $searchDir

	echo "Begin majority rule annotation: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
	for i in $(ls $MRout | grep "pfmatch")
		do
		name=$(basename $i .pfmatch)
		python $pipelineDir/bin/MRannotation.py $MRout/${name}.bl $MRout/${name}.blx $MRout/${name}.bln $MRout/${name}.pfmatch $MRout/${name}.repeatmodeler $MRout $MRout/MRannotations.out $dir/BLASTN/DB/genomes_contig.fofn
	done
	echo "End of majority rule annotation: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"

fi

echo "Begin index of annotated TE sequences: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
# Index unclassified TEs as any other annotation
for i in $(ls $dir/annotation/out/*.out | egrep -v "MRannotations.out|unclass.out")
	do
	gName=$(basename $i .out)
	awk -v OFS="\t" '{if($2<$3){start=$2-1; end=$3-1} else {start=$3-1; end=$2-1}; split($1,a,":"); print a[1]"__"$4,start,end,$5}' $i | bedtools sort | bedtools cluster |\
		awk 'BEGIN{suf=0; cpt=1}{seen[$5]++; split($1,contigAnnot,"__"); if(cpt!=$5){cpt++; suf=0} else {suf++};\
		cluster[$5"_"suf]=contigAnnot[1]"\t"$2"\t"$3"\t"contigAnnot[2]"_"suf"\t"$4; solo[$5"_"suf]=contigAnnot[1]"\t"$2"\t"$3"\t"contigAnnot[2]"\t"$4}END{\
		for (c in cluster){split(c,ind,"_"); if(seen[ind[1]]==1){print solo[c]} else {print cluster[c]}}}' | sort -k1,1 -k2,2n -k3,3n -k4,4 > $dir/annotation/index/$gName.out
done
echo "End of index of annotated TE sequences: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"


##bp annotation

detectionDir=$dir/bpAnnotation

# Prepare bp annotation
rm -f $detectionDir/run_parallel.sh

for i in $(ls $dir/annotation/index/*.out | grep -v "unclass")
	do
	# For each indexed annotation, cut the index and the corresponding genome files according to divover
	# First split by contig
	iName=$(basename $i .out)
	awk -v out=$detectionDir/contigIndex '{print $0 >> out"/"$1".out"}' $i
	# Prepare script to run python bp annotation in parallel for genomes
	genome=$(grep "$iName" $dir/BLASTN/DB/genomes_contig.fofn | awk 'NR==1 {print $1}')
	awk '{if($0~/^>/){if(id!=""){print id"\t"seq}; id=$1; seq=""} else {seq=seq""$0}}END{print id"\t"seq}' $genome |\
		awk -v out=$detectionDir/genome '{a=$1; gsub(">","",a); print $1"\n"$2 >> out"/"a".fa"}'

	# Then split contigs
	cName=$(grep "$iName" $dir/BLASTN/DB/genomes_contig.fofn | awk 'NR==1 {print $2}')
	for j in $(ls $detectionDir/genome/${cName}_*.fa)
		do
		jName=$(basename $j .fa)
		if [ -f $detectionDir/contigIndex/${jName}.out ]
			then
			jSize=$(awk '$0!~/^>/ {print length($0)}' $j);
			if [ $jSize -gt 10000 ]
				then
				# cut the genome contig file according to divover (also the overlap between contigs) and prepare a bed file to intersect with the index file
				awk -v detectionDir=$detectionDir -v contig=$jName -v jSize=$jSize -v suf=1 -v divover=30 '{if($0~/^>/){id=$0} else {len=split($0,seq,""); \
					cpt=1; for(i=1;i<=len;i++){ \
					if(cpt>jSize/divover && suf<divover-1){print id"\n"start""s >> detectionDir"/cutGenome/"contig"__"suf".fa"; \
					if(suf==1){print contig"\t1\t"length(s)"\t"contig"__"suf >> detectionDir"/cutGenome/"contig".gff"} else { \
					print contig"\t"i-length(s)-length(start)-2"\t"i-2"\t"contig"__"suf >> detectionDir"/cutGenome/"contig".gff"}; \
					start=""; l=split(s,a,""); for (j=l-divover;j<l;j++){start=start""a[j]}; suf++; cpt=0; s=""} else {s=s""seq[i]}; cpt++}}}END{ \
					print id"\n"start""s >> detectionDir"/cutGenome/"contig"__"suf".fa"; \
					print contig"\t"i-length(s)-length(start)-2"\t"i-1"\t"contig"__"suf >> detectionDir"/cutGenome/"contig".gff"}' $j

				# split contigIndex according to split genome (beware, replace coordinate of TE when overlapping two contigs to fit the matrix)
				bedtools intersect -wa -wb -a <(bedtools sort -i $detectionDir/contigIndex/${jName}.out) -b $detectionDir/cutGenome/${jName}.gff |\
						awk -v detectionDir=$detectionDir -v OFS="\t" '{if($2<$7){start=$7} else {start=$2}; if($3>$8){end=$8} else {end=$3}; \
						print $1,start,end,$4,$5 >> detectionDir"/contigIndex/"$NF".out"}'
				# remove the unsplit contig index, else it's kept
				rm $detectionDir/contigIndex/${jName}.out
			else
				awk -v OFS="\t" '{if($0~/^>/){contig=$1; gsub(">","",contig)} else {end=length($0); print contig,1,end,contig}}' $j > $detectionDir/cutGenome/${jName}.gff
				cp $j $detectionDir/cutGenome/${jName}.fa
			fi
		fi
	done

	# Start bp annotation on cut contigs and cut contig indexes
	for k in $(ls $detectionDir/contigIndex/${cName}*.out)
		do
		kName=$(basename $k .out)
		kSubName=$(echo $kName | awk '{split($1,a,"__"); print a[1]}')
		nbAnnot=$(awk '{seen[$4]++}END{for (s in seen){print s}}' $k | wc -l)
		decay=$(grep "$kName$" $detectionDir/cutGenome/${kSubName}.gff | awk '{print $2-1}' | sort -k1,1 | head -1)
		echo "python2.7 $pipelineDir/bin/bpAnnotation.py $detectionDir/cutGenome/${kName}.fa $k $nbAnnot $decay > $detectionDir/tmp/${kName}_NT.gff3" >> $detectionDir/run_parallel.sh
	done
done


echo "Begin base pair annotation: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"
# Run bp annotation in parallel for genomes
$PARALLEL -j $nbProc < ${detectionDir}/run_parallel.sh


# Post-treatment of bp annotation

# merge all contig annotation by genome
while read line
	do
	gName=$(echo $line | awk '{len=split($1,a,"/"); print a[len]}' | sed 's/.fa//g')
	cName=$(echo $line | awk '{print $2}')
	cat $detectionDir/tmp/${cName}* | sort -k1,1 -k2,2n > $detectionDir/out/${gName}_NT.gff3
done < $dir/BLASTN/DB/genomes_contig.fofn

# Transform bp annotation into regular annotation
for i in $(ls $detectionDir/out/*_NT.gff3)
	do
	iName=$(basename $i .gff3 | sed 's/_NT//g');
	awk '{seen[NR]=$0} END {for (i=1; i<=NR; i++) {print seen[i]"\t"seen[i+1]}}' $i |\
		awk '{if (($1==$5 && $4!=$8) || ($1==$5 && $6-$2>1) || ($1!=$5)) {print $1"\t"$2"\t"$4"\t"$5"\t"$6"\t"$8} else {\
		print $1"\t"$2"\t"$4}}END{print $1"\t"$2"\t"$4}' |\
		awk '{split($3,a,"_"); if (NR==1) {deb=$2} if(NF>3){print $1"\tMicroTEp\t.\t"deb"\t"$2"\t.\t.\t.\t"a[1]; deb=$5}}END{\
		split($3,a,"_"); print $1"\tMicroTEp\t.\t"deb"\t"$2"\t.\t.\t.\t"a[1]}' > $detectionDir/out/${iName}_tmp.gff3
	rm $i
done

# Add unclassified TE annotations
for i in $(ls $detectionDir/out/*_tmp.gff3)
	do
	iName=$(basename $i _tmp.gff3)
	cat <(bedtools intersect -v -a <(awk '{split($1,a,":"); seen[a[1]"\t"$2"\t"$3"\t"$4]++}END{for (s in seen) print s}' $dir/annotation/out/${iName}_unclass.out  | bedtools sort) -b <(awk -v OFS="\t" '{print $1,$4,$5,$9}' $i | bedtools sort) |\
	bedtools merge | awk -v OFS="\t" '{print $1,"MicroTEp",".",$2,$3,".",".",".","unclassified:unclassified:unclassified"}') $i | sort -k1,1 -k2,2n > $detectionDir/out/${iName}.gff3
	rm $i
done

echo "End of base pair annotation: $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"

# Remove unclassified TEs overlapping genes (if geneDir provided)
if $jflag
	then
	for bpannot in $(ls $detectionDir/out/*.gff3 | egrep -v "tmp|_NT")
		do
		name=$(basename $bpannot .gff3)
		grep "unclassified" $bpannot | awk -v OFS="\t" '{print $1,$4,$5}' | bedtools sort -i > $dir/bpAnnotation/geneFilter/bed/${name}_unclassified.bed
		bedtools intersect -wb -a <(bedtools sort -i $geneDir/${name}.bed) -b $dir/bpAnnotation/geneFilter/bed/${name}_unclassified.bed | awk -v OFS="\t" '{print $4,$5,$6}' > $detectionDir/geneFilter/bed/${name}_unclassified_overlappingGenes.bed
		bedtools intersect -v -a $bpannot -b $detectionDir/geneFilter/bed/${name}_unclassified_overlappingGenes.bed > $detectionDir/geneFilter/${name}.gff3
	done
fi

rm -r $detectionDir/tmp
rm -r $detectionDir/contigIndex
rm -r $detectionDir/cutGenome

echo "End of MicroTEp ! $(date -R | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}')"

