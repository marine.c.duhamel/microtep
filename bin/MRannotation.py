#!/usr/bin/env python
#-*- coding : utf8 -*-

'''
Author: Marine Duhamel
Contact: marine.c.duhamel@gmail.com
Date: 05/01/20
'''

#import
import sys
import operator


# Arguments

blastFile=sys.argv[1] #filtered and resumed TE blast file
blxFile = sys.argv[2] #blastX file
blnFile = sys.argv[3] #blastN file
pfmatchFile = sys.argv[4] #pfamscan match file
repeatmodelerFile = sys.argv[5] #repeatmodeler file
outFile = sys.argv[6] # directory of MR annotation result for bp annotation, incomplete, need name of genome (obtained with corrTable)
allAnnotFile = sys.argv[7] # MR annotation result file
corrTable = sys.argv[8] # Table with genome/contig matches

# Get annotations from blastx
d_blx = {} # dic of blx results, key is sequence id, value is a dic with annotation as key and occurrence as value
blx = open(blxFile, "r")
for line in blx:
        seq = line.split("\t")[0] # first field is the sequence id
        completeAnnot = line.split("\t")[1].split(":") # second field is the complete annotation
        annot = completeAnnot[1] + ":" + completeAnnot[2] + ":" + completeAnnot[3] # annot is only class:order:fam, what we want
	# Add one occurrancy of this annotation to the sequence dic, create the dic of the sequence and the corresponding annotation if not existing
        if seq in d_blx.keys() :
                if annot in d_blx[seq].keys() :
                        d_blx[seq][annot]+=1
                else :
                        d_blx[seq][annot]=1
        else :
                d_blx[seq]={}
                d_blx[seq][annot]=1
blx.close()


# Get annotations from blastn
d_bln = {} # dic of bln results, key is sequence id, value is a dic with annotation as key and occurrence as value
bln = open(blnFile, "r")
for line in bln:
        seq = line.split("\t")[0] # first field is the sequence id
        completeAnnot = line.split("\t")[1].split(":") # second field is the complete annotation
        annot = completeAnnot[1] + ":" + completeAnnot[2] + ":" + completeAnnot[3] # annot is only class:order:fam, what we want
	# Add one occurrancy of this annotation to the sequence dic, create the dic of the sequence and the corresponding annotation if not existing
        if seq in d_bln.keys() :
                if annot in d_bln[seq].keys() :
                        d_bln[seq][annot]+=1
                else :
                        d_bln[seq][annot]=1
        else :
                d_bln[seq]={}
                d_bln[seq][annot]=1
bln.close()


# Get annotation from pfamscan matches
d_pf = {} # dic of pfamscan results, key is sequence id, value is a dic with annotation as key and occurrence as value
pf = open(pfmatchFile,"r")
for line in pf:
	seq = line.split("\t")[0] # first field is the sequence id
	annot = line.split("\t")[1] # second field in the annotation found by pfamscan
	occurrence = line.split("\t")[2].rstrip("\n")
	# Add one occurrancy of this annotation to the sequence dic, create the dic of the sequence and the corresponding annotation if not existing
	if seq in d_pf.keys() :
		if annot in d_pf[seq].keys() :
			d_pf[seq][annot]+=int(occurrence)
		else :
			d_pf[seq][annot]=int(occurrence)
	else :
		d_pf[seq]={}
		d_pf[seq][annot]=int(occurrence)
pf.close()

# Get annotation from repeatmodeler
d_repeatmodeler = {} # dic of repeatmodeler results, key is sequence id, value is a dic with annotation as key and occurrence as value
repeatmodeler = open(repeatmodelerFile, "r")
for line in repeatmodeler:
	seq = line.split("\t")[0] # first field is the sequence id
	annot = line.split("\t")[1].rstrip("\n") # second field in the annotation
	# Add one occurrancy of this annotation to the sequence dic, create the dic of the sequence and the corresponding annotation if not existing
	if seq in d_repeatmodeler.keys() :
		if annot in d_repeatmodeler[seq].keys() :
			d_repeatmodeler[seq][annot]+=1
		else :
			d_repeatmodeler[seq][annot]=1
	else :
		d_repeatmodeler[seq]={}
		d_repeatmodeler[seq][annot]=1

repeatmodeler.close()

# Get correspondancy for contig name and genome name
d_corr = {}
corr = open(corrTable,"r")
for line in corr:
	genome = line.split("\t")[0].split("/")[-1].replace(".fa","")
	contig = line.split("\t")[1].rstrip("\n")
	d_corr[contig] = genome

corr.close()

# For each sequence in the blast filtered and resumed result, get the annotation by MR
bl = open(blastFile, "r") # the resumed blast filtered result
allAnnot = open(allAnnotFile, "a") # the MR annotation output file
for line in bl :
	contig = line.split("\t")[0] # first field is the contig
	shortContig=contig.split("_")[0]
	if int(line.split("\t")[1]) < int(line.split("\t")[2]):
		start = line.split("\t")[1] # 2nd field is the start
		end = line.split("\t")[2] #3rd field is the end
	else:
		end = line.split("\t")[1] # 2nd field is the end
		start = line.split("\t")[2] #3rd field is the start
	bitscore = line.split("\t")[3] # 4th field is the bitscore, essential for bp annotation
	id=contig + ":" + start + "-" + end # sequence id
	outclass = open(outFile + "/" + d_corr[shortContig] + ".out", "a") # the output file for bp annotation (classified)
	outunclass = open(outFile + "/" + d_corr[shortContig] + "_unclass.out", "a") # the output file for bp annotation (unclassified)
	d_result = {} # dic result contain the MR annotation for each method of annotation
	# if id is a key of d_blx, meaning the blastx annotation search provided an annotation, use MR to annotate the sequence
	if id in d_blx.keys() :
		MR=d_blx[id][max(d_blx[id].items(), key=operator.itemgetter(1))[0]] # the occurrence number of the major annotation
		blx_result = [k for k,v in d_blx[id].items() if v==MR] # find the annotation corresponding to highest occurrence value
		# if there is only one, put simple annotation in result list
		if len(blx_result) == 1 :
			if max(d_blx[id].items(),key=operator.itemgetter(1))[0] in d_result.keys() :
				d_result[max(d_blx[id].items(),key=operator.itemgetter(1))[0]]+=1
			else :
				d_result[max(d_blx[id].items(),key=operator.itemgetter(1))[0]]=1
		# Else put each possible annotation in result list
		else :
			for blxres in blx_result:
				if blxres in d_result.keys() :
					d_result[blxres]+=1
				else :
					d_result[blxres]=1

	# if id is a key of d_bln, meaning the blastn annotation search provided an annotation, use MR to annotate the sequence
	if id in d_bln.keys() :
		MR=d_bln[id][max(d_bln[id].items(), key=operator.itemgetter(1))[0]] # the occurrence number of the major annotation
		bln_result = [k for k,v in d_bln[id].items() if v==MR] # find the annotation corresponding to highest occurrence value
		# if there is only one, put simple annotation in result list
		if len(bln_result) == 1 :
			if max(d_bln[id].items(),key=operator.itemgetter(1))[0] in d_result.keys() :
				d_result[max(d_bln[id].items(),key=operator.itemgetter(1))[0]]+=1
			else :
				d_result[max(d_bln[id].items(),key=operator.itemgetter(1))[0]]=1
		# Else put each possible annotation in result list
		else :
			for blnres in bln_result:
				if blnres in d_result.keys():
					d_result[blnres]+=1
				else :
					d_result[blnres]=1

	# if id is a key of d_pf, meaning the pfamscan annotation search provided an annotation, use MR to annotate the sequence
	if id in d_pf.keys() :
		MR=d_pf[id][max(d_pf[id].items(), key=operator.itemgetter(1))[0]] # the occurrece number of the major annotation
		pf_result = [k for k,v in d_pf[id].items() if v==MR] # find the annotation corresponding to highest occurrence value
		# if there is only one, put simple annotation in result list
		if len(pf_result) == 1 :
			if max(d_pf[id].items(),key=operator.itemgetter(1))[0] in d_result.keys() :
				d_result[max(d_pf[id].items(),key=operator.itemgetter(1))[0]]+=1
			else :
				d_result[max(d_pf[id].items(),key=operator.itemgetter(1))[0]]=1
		# Else put each possible annotation in result list
		else :
			for pfres in pf_result:
				if pfres in d_result.keys():
					d_result[pfres]+=1
				else:
					d_result[pfres]=1

	# if id is a key of d_repeatmodeler, meaning the repeatmodeler provided an annotation, use MR to annotate the sequence
	if id in d_repeatmodeler.keys() :
		MR=d_repeatmodeler[id][max(d_repeatmodeler[id].items(), key=operator.itemgetter(1))[0]] # the occurrence number of the major annotation
		repeatmodeler_result = [k for k,v in d_repeatmodeler[id].items() if v==MR] # find the annotation corresponding to highest occurrence value
		# if there is only one, put simple annotation in result list
		if len(repeatmodeler_result) == 1 :
			if max(d_repeatmodeler[id].items(),key=operator.itemgetter(1))[0] in d_result.keys() :
				d_result[max(d_repeatmodeler[id].items(),key=operator.itemgetter(1))[0]]+=1
			else :
				d_result[max(d_repeatmodeler[id].items(),key=operator.itemgetter(1))[0]]=1
		# Else put each possible annotation in result list
		else :
			for rmres in repeatmodeler_result:
				if rmres in d_result.keys():
					d_result[rmres]+=1
				else :
					d_result[rmres]=1


	if not d_result :
		allAnnot.write(id + "\tunclassified:unclassified:unclassified\n") # for MR annotation file
		outunclass.write(id + "\t" + start + "\t" + end + "\tunclassified:unclassified:unclassified\n") # to add after bp annotation
	else :
		# Find major annotation according to the 4 methods
		MR=d_result[max(d_result.items(), key=operator.itemgetter(1))[0]] # the occurrece number of the major annotation for all the 4 methods
		MR_result = [k for k,v in d_result.items() if v==MR]
		# if a single annotation is found by MR, the sequence is annotated with this annotation
		if len(MR_result) == 1 :
			allAnnot.write(id + "\t" + max(d_result.items(), key=operator.itemgetter(1))[0] + "\n") # for MR annotation file
			outclass.write(id + "\t" + start + "\t" + end + "\t" + max(d_result.items(), key=operator.itemgetter(1))[0] + "\t" + bitscore) # for bp annotation
		# if multiple annotation were found equal for a sequence, the sequence is annotated as MultipleAnnotation
		else :
			allAnnot.write(id + "\tMultipleAnnotations\n") # for MR annotation file
			outclass.write(id + "\t" + start + "\t" + end + "\tMultipleAnnotations\t" + bitscore) # for bp annotation

bl.close()
allAnnot.close()
